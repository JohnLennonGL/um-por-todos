package com.jlngls.appsem1.ui.midiaplayer;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintsChangedListener;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;

import com.jlngls.appsem1.R;

import java.io.FileDescriptor;
import java.io.PrintWriter;

import static androidx.core.content.ContextCompat.getSystemService;

/**
 * A simple {@link Fragment} subclass.
 */
public class MidiaPlayerFragment extends Fragment implements View.OnClickListener , SeekBar.OnSeekBarChangeListener {


    private MediaPlayer mediaPlayer;
    private ImageView btnPlayerFM,btnPauseFM,btnStopFM;
    private SeekBar seekBarVolLM;
    private AudioManager audioManager;

    public MidiaPlayerFragment(){

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate( R.layout.fragment_midiaplayer, container, false );

        mediaPlayer = MediaPlayer.create(  getActivity(),R.raw.teste);


        btnPlayerFM = view.findViewById( R.id.btnPlayerFM );
        btnPauseFM = view.findViewById( R.id.btnPauseFM );
        btnStopFM = view.findViewById( R.id.btnStopFM );
        seekBarVolLM = view.findViewById( R.id.seekBarVolFM );


        //Configurar click dos botoes no fragment Obs(NUNCA ESQUECA DE IMPLENTAR O METODO DE CLICK NA CLASSE)
        btnPlayerFM.setOnClickListener(this);
        btnPauseFM.setOnClickListener(this);
        btnStopFM.setOnClickListener(this);
        seekBarVolLM.setOnSeekBarChangeListener(this);

        controlarVolume();

         return view;
    }


    @Override
    public void onClick(View view) {
        // VERIFICANDO SE O ID É O IGUAL ID DO ITEM QUE EU QUERO.
        switch (view.getId()) {
            case R.id.btnPlayerFM:
                executarSom();
                break;
        }
        switch (view.getId()){
            case R.id.btnPauseFM:
                pausarSom();
                break;
        }
        switch (view.getId()){
            case R.id.btnStopFM:
                pararSom();
                break;
        }

    }

    private void executarSom() {
        if(mediaPlayer != null){
            mediaPlayer.start();
        }
    }
    private void pausarSom(){
        if(mediaPlayer.isPlaying()){
            mediaPlayer.pause();
        }
    }
    private void pararSom(){
        if(mediaPlayer.isPlaying()){
            mediaPlayer.stop();
            mediaPlayer = MediaPlayer.create( getActivity(),R.raw.teste);
        }
    }

    public void controlarVolume() {

        // configurar audio manager
        audioManager = (AudioManager) getActivity().getSystemService( Context.AUDIO_SERVICE );

        // recuperar Valor do Volume maximo e o volume ataul
        int volMax = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        int volAtual = audioManager.getStreamVolume( AudioManager.STREAM_MUSIC );

        //configurar os valores maximos para o seekbar
        seekBarVolLM.setMax( volMax );

        //configurar os valores atual para o seekbar
        seekBarVolLM.setProgress( volAtual );



    }




    //mover seekbar
    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        audioManager.setStreamVolume( AudioManager.STREAM_MUSIC,progress, 0);//AudioManager.FLAG_SHOW_UI);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }


    @Override
    public void onStop() {
        super.onStop();
        if(mediaPlayer.isPlaying()) {
            mediaPlayer.pause();

        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(mediaPlayer.isPlaying() && mediaPlayer != null){
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;

        }
    }
}