package com.jlngls.appsem1.ui.execVideo;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.jlngls.appsem1.PlayerVideoActivity;
import com.jlngls.appsem1.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ExecVideoFragment extends Fragment implements View.OnClickListener {

    private ImageView btnImgExecFE;

    public ExecVideoFragment(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate( R.layout.fragment_execvideo, container, false );

        btnImgExecFE = view.findViewById( R.id.btnImgExecFE );

        btnImgExecFE.setOnClickListener( this );

        return  view;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.btnImgExecFE:
                startActivity( new Intent( getActivity(), PlayerVideoActivity.class ));
                break;

        }
    }
}